using System.Net.Http.Headers;
using System.Text;

namespace JsonRpcClient.Clients;

public class RpcHttpClient : RpcClient
{
    private readonly HttpClient _client = new();
    private readonly HttpClientHandler _handler = new HttpClientHandler();

    protected RpcHttpClient(string baseUri, RpcClientOptions? options = null)
    {
        if (options != null)
        {
            if (!options.ValidateRemoteCertificate)
            {
                _handler.ServerCertificateCustomValidationCallback =
                    HttpClientHandler.DangerousAcceptAnyServerCertificateValidator;
            }

            _client = new HttpClient(_handler);
        }

        _client.BaseAddress = new Uri(baseUri);
        _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
    }

    protected override async Task<string> SendAndGetJson(string request)
    {
        var response = await _client.PostAsync("", new StringContent(request, Encoding.UTF8, "application/json"));
        return await response.Content.ReadAsStringAsync();
    }

    protected override async Task SendJson(string request)
    {
        await _client.PostAsync("", new StringContent(request, Encoding.UTF8, "application/json"));
    }
}