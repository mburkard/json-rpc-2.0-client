﻿namespace JsonRpcClient.Clients
{
    public class RpcClientOptions
    {
        public bool ValidateRemoteCertificate { get; set; } = true;
    }
}